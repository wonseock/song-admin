package com.song.admin.api.controller;

import com.song.admin.api.service.ApiSampleService;
import com.song.admin.entity.Sample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("test")
public class TestController {

    @Autowired
    private ApiSampleService apiSampleService;

    @GetMapping
    public List<Sample> getSamples() {
        return apiSampleService.getSamples();
    }

    @GetMapping("{sampleId}")
    public Sample getSample(@PathVariable Long sampleId) {
        return apiSampleService.getSample(sampleId);
    }

    @PostMapping
    public Sample setSample(
            @RequestBody Sample command
    ) {
        return apiSampleService.setSample(command);
    }

    @PutMapping("{sampleId}")
    public Sample putSample(
            @PathVariable Long sampleId,
            @RequestBody Sample command
    ) {
        return apiSampleService.putSample(sampleId, command);
    }

    @DeleteMapping("{sampleId}")
    public void delSample(
            @PathVariable Long sampleId
    ) {
        apiSampleService.delSample(sampleId);
    }

}
